# Build:
# docker build -t shallawell/bears-mean-api .
#
# Run:
# docker run -it -p 8000:8000 -p 27017:27017 -p 28017:28017  shallawell/bears-mean-api
#
# Compose:
# docker-compose up -d

FROM ubuntu:latest
MAINTAINER @shallawell

# Install Utilities
RUN apt-get update -q
RUN apt-get install -yqq wget aptitude git curl sudo nano gcc build-essential libfreetype6 libfontconfig libkrb5-dev supervisor
# Supervisor is used to manage Mongod and node
# copy the local supervisord.conf file to the container
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf


# Install gem sass for grunt-contrib-sass
#RUN apt-get install -y ruby
#RUN gem install sass

# Install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
RUN apt-get install -yq nodejs

# Install MEAN.JS Prerequisites
RUN npm install --quiet -g grunt-cli gulp mocha nodemon

#RUN mkdir /opt/
#RUN mkdir -p /opt/bears-mean-api/public/lib
WORKDIR /opt/

# Copies the local package.json file to the container
# and utilities docker container cache to not needing to rebuild
# and install node_modules/ everytime we build the docker, but only
# when the local package.json file changes.
# Install npm packages
#ADD package.json /opt/bears-mean-api/package.json
#RUN npm install --quiet

#OR
# get bears-mean-api from gitlab repo
RUN git clone https://shallawell@gitlab.com/shallawell/bears-mean-api.git

# change to new cloned dir
WORKDIR /opt/bears-mean-api/
# install app pre-req (like express, mongoose, etc)
RUN npm install

# Share local directory on the docker container
ADD . /opt/bears-mean-api

### MongoDB
# make mongodb volumes
RUN mkdir /data && mkdir /data/db
# Install MongoDB.
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
RUN echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' > /etc/apt/sources.list.d/mongodb.list
RUN apt-get update -yq
RUN apt-get install -yqq mongodb-org

# Define mountable directories.
VOLUME ["/data/db"]


# Machine cleanup
RUN npm cache clean
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set development environment as default
ENV NODE_ENV development

# Port 8000 for bears-mean-api MEAN.JS server
EXPOSE 8000:8000

# Port 5858 for node debug
#EXPOSE 5858:5858

# Expose mongodb ports.
#   - 27017: process
#   - 28017: http
EXPOSE 27017:27017
EXPOSE 28017:28017

# Run supervisord server to start mongod and bears-mean-api
CMD ["/usr/bin/supervisord"]
# CMD ["/bin/bash"]